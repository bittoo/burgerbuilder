import React from "react";
import Classes from "./Input.module.css";

const Input = (props) => {
  let inputElement = null;
  const inputClasses = [Classes.InputElement];

  if (props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push(Classes.Invalid);
  }

  switch (props.elementType) {
    case "input":
      inputElement = (
        <input
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;

    case "textarea":
      inputElement = (
        <textarea
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;

    case "select":
      inputElement = (
        <select
          className={inputClasses.join(" ")}
          value={props.value}
          onChange={props.changed}
        >
          {props.elementConfig.options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;

    default:
      inputElement = (
        <input
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;
  }

  let validationErrMsg = null;
  if (props.invalid && props.touched) {
    validationErrMsg = (
      <p className={Classes.ValidationError}>Please enter a valid value!</p>
    );
  }

  return (
    <div className={Classes.Input}>
      <label className={Classes.Label}>{props.label}</label>
      {inputElement}
      {validationErrMsg}
    </div>
  );
};

export default Input;
