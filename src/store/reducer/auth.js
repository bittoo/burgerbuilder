import * as actionTypes from "../action/actionTypes";

const initialState = {
  userId: null,
  token: null,
  error: null,
  loading: false,
  authRedirectUrl: "/"
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return {
        ...state,
        error: null,
        loading: true
      };

    case actionTypes.AUTH_SUCCESS:
      return {
        ...state,
        userId: action.data.localId,
        token: action.data.idToken,
        error: null,
        loading: false
      };

    case actionTypes.AUTH_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error
      };

    case actionTypes.AUTH_LOGOUT:
      return {
        ...state,
        userId: null,
        token: null
      };

    case actionTypes.SET_AUTH_REDIRECT_URL:
      return {
        ...state,
        authRedirectUrl: action.path
      };

    default:
      return state;
  }
};

export default reducer;
