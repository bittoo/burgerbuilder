import axios from "axios";

const instance = axios.create({
  baseURL: "https://react-burger-builder-58a6c.firebaseio.com/"
});

export default instance;
